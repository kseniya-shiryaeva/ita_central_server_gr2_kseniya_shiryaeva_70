<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\RestPlace;
use App\Model\Client\ClientHandler;
use App\Model\RestPlace\RestPlaceHandler;
use App\Repository\ClientRepository;
use App\Repository\RestPlaceRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RestPlaceController extends Controller
{
    /**
     * @Route("/rest_place", name="rest_place")
     *
     * @param RestPlaceRepository $restPlaceRepository
     * @param RestPlaceHandler $restPlaceHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createRestPlaceAction(
        RestPlaceRepository $restPlaceRepository,
        RestPlaceHandler $restPlaceHandler,
        ObjectManager $manager,
        Request $request,
    ClientRepository $clientRepository
    )
    {
        $data['type'] = $request->request->get('type');
        $data['landlord'] = $clientRepository->findOneByEmail($request->request->get('landlord'));
        $data['placeName'] = $request->request->get('placeName');
        $data['apartmentCount'] = $request->request->get('apartmentCount');
        $data['contactPerson'] = $request->request->get('contactPerson');
        $data['phone'] = $request->request->get('phone');
        $data['address'] = $request->request->get('address');
        $data['price'] = $request->request->get('price');

        if($data['type'] == 'sanatorium'){
            $data['excursion'] = $request->request->get('excursion');
            $data['treatment'] = $request->request->get('treatment');
            $restPlace = $restPlaceHandler->createNewSanatorium($data);
        } elseif ($data['type'] == 'hotel'){
            $data['kitchen'] = $request->request->get('kitchen');
            $data['selfPlage'] = $request->request->get('selfPlage');
            $restPlace = $restPlaceHandler->createNewHotel($data);
        }

        if(empty($data['placeName']) || empty($data['landlord']) || empty($data['apartmentCount'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

       if(!empty($restPlace)){
           $manager->persist($restPlace);
           $manager->flush();
       }


        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/rest_places", name="rest_places")
     *
     * @param RestPlaceRepository $restPlaceRepository
     * @return JsonResponse
     */
    public function restPlacesAction(
        RestPlaceRepository $restPlaceRepository
    ){
        /**
         * @var RestPlace[]
         */
        $places = $restPlaceRepository -> findAll();

        $result = [];

        foreach ($places as $place){
            $result[] = $place->__toArray();
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function restPlaceByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $user = $clientRepository->findOneByEmail($email);
        if ($user) {
            return new JsonResponse($user->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }
}
