<?php

namespace App\Controller;

use App\Repository\BookingRepository;
use App\Entity\RestPlace;
use App\Model\Booking\BookingHandler;
use App\Model\RestPlace\RestPlaceHandler;
use App\Repository\ClientRepository;
use App\Repository\RestPlaceRepository;
use App\Repository\TenantRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends Controller
{
    /**
     * @Route("/booking", name="booking")
     *
     * @Method("POST")
     * @param ObjectManager $manager
     * @param Request $request
     * @param TenantRepository $tenantRepository
     * @param BookingHandler $bookingHandler
     * @param RestPlaceRepository $restPlaceRepository
     * @return JsonResponse
     */
    public function createBookingAction(
        ObjectManager $manager,
        Request $request,
        TenantRepository $tenantRepository,
        BookingHandler $bookingHandler,
        RestPlaceRepository $restPlaceRepository
    )
    {
        $data['restPlace'] = $restPlaceRepository->find($request->request->get('restPlace'));
        $data['apartment'] = $request->request->get('apartment');
        $data['tenant'] = $tenantRepository->findOneByEmail($request->request->get('tenant'));
        $data['startDate'] = $request->request->get('startDate');


        if(empty($data['restPlace']) || empty($data['apartment']) || empty($data['tenant'])) {
            return new JsonResponse(['error' => 'Недостаточно данных'],406);
        }

        $booking = $bookingHandler->createNewBooking($data);

       if(!empty($booking)){
           $manager->persist($booking);
           $manager->flush();
       }


        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/check_booking/{email}", name="check_booking")
     *
     * @param string $email
     * @param ClientRepository $clientRepository
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function checkBookingByEmailAction(
        string $email,
        ClientRepository $clientRepository,
        BookingRepository $bookingRepository
    )
    {

        $tenant = $clientRepository->findOneByEmail($email)->getId();

        if ($bookingRepository->findOneByTenant($tenant)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/booking_variants/{restPlace}", name="booking_variants")
     *
     * @param int $restPlace
     * @param BookingRepository $bookingRepository
     * @return JsonResponse
     */
    public function bookingVariantAction(int $restPlace, BookingRepository $bookingRepository)
    {
       $dates = $bookingRepository->getDatesByRestPlace($restPlace);

        if($dates){
            return new JsonResponse($dates);
        } else {
            throw new NotFoundHttpException();
        }


    }
}
