<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 18.06.18
 * Time: 16:23
 */

namespace App\Model\Booking;

use App\Entity\Booking;

class BookingHandler
{


    /**
     * @param array $data
     * @return Booking
     */
    public function createNewBooking(array $data) {
        $booking = new Booking();
        $booking->setRestPlace($data['restPlace'])
            ->setApartment($data['apartment'])
            ->setTenant($data['tenant'])
            ->setStartDate($data['startDate'])
            ->setEndDate(date("Y-m-d H:i:s", (strtotime($data['startDate'])+3600*24*6)));

        return $booking;
    }
}
