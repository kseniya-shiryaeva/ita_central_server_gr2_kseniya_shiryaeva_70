<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 18.06.18
 * Time: 16:23
 */

namespace App\Model\RestPlace;

use App\Entity\Client;
use App\Entity\Hotel;
use App\Entity\RestPlace;
use App\Entity\Sanatorium;

class RestPlaceHandler
{


    /**
     * @param array $data
     * @return RestPlace
     */
    public function createNewRestPlace(array $data) {
        $restPlace = new RestPlace();
        $this->createNewAbstractRestPlace($restPlace, $data);

        return $restPlace;
    }

    /**
     * @param RestPlace $restPlace
     * @param array $data
     * @return RestPlace
     */
    public function createNewAbstractRestPlace(RestPlace $restPlace, array $data) {
        $restPlace->setLandlord($data['landlord']);
        $restPlace->setPlaceName($data['placeName']);
        $restPlace->setApartmentCount($data['apartmentCount']);
        $restPlace->setContactPerson($data['contactPerson']);
        $restPlace->setPhone($data['phone']);
        $restPlace->setAddress($data['address']);
        $restPlace->setPrice($data['price']);
        $restPlace->setType($data['type']);

        return $restPlace;
    }

    /**
     * @param array $data
     * @return Sanatorium
     */
    public function createNewSanatorium(array $data) {
        /** @var Sanatorium $restPlace */
        $restPlace = $this->createNewAbstractRestPlace(new Sanatorium(), $data);
        $restPlace->setExcursion($data['excursion'] ?? 0);
        $restPlace->setTreatment($data['treatment'] ?? 0);

        return $restPlace;
    }

    /**
     * @param array $data
     * @return Hotel
     */
    public function createNewHotel(array $data) {
        /** @var Hotel $restPlace */
        $restPlace = $this->createNewAbstractRestPlace(new Hotel(), $data);
        $restPlace->setKitchen($data['kitchen'] ?? 0);
        $restPlace->setSelfPlage($data['selfPlage'] ?? 0);

        return $restPlace;
    }
}
