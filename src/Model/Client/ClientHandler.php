<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 18.06.18
 * Time: 16:23
 */

namespace App\Model\Client;

use App\Entity\Client;
use App\Entity\Landlord;
use App\Entity\Tenant;

class ClientHandler
{


    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $this->createNewAbstractClient($client, $data);

        return $client;
    }

    /**
     * @param array $data
     * @param Client $client
     * @return Client
     */
    public function createNewAbstractClient(Client $client, array $data) {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePlainPassword($data['password']);
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setRoles($data['roles'] ?? ['ROLE_USER']);

        return $client;
    }

    /**
     * @param array $data
     * @return Tenant
     */
    public function createNewTenant(array $data) {
        /** @var Tenant $client */
        $client = $this->createNewAbstractClient(new Tenant(), $data);
        $client->setCreditCard($data['credit_card']??null);

        return $client;
    }

    /**
     * @param array $data
     * @return Landlord
     */
    public function createNewLandlord(array $data) {
        /** @var Landlord $client */
        $client = $this->createNewAbstractClient(new Landlord(), $data);
        $client->setFullName($data['full_name']??null);

        return $client;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password) . md5($password . '2');
    }
}
