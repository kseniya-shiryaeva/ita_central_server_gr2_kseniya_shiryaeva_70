<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 02.07.18
 * Time: 13:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;


/**
 * @ORM\Entity(repositoryClass="App\Repository\RestPlaceRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"rest_place" = "RestPlace", "sanatorium" = "Sanatorium", "hotel" = "Hotel"})
 */
class RestPlace
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Landlord", inversedBy="restPlaces")
     */
    private $landlord;

    /**
     * @var string
     * @ORM\Column(type="string", length=254)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $placeName;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $apartmentCount;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $contactPerson;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $address;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="restPlace")
     */
    private $bookings;

    /**
     * @param string $type
     * @return RestPlace
     */
    public function setType(string $type): RestPlace
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $placeName
     * @return RestPlace
     */
    public function setPlaceName(string $placeName): RestPlace
    {
        $this->placeName = $placeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    /**
     * @param int $apartmentCount
     * @return RestPlace
     */
    public function setApartmentCount(int $apartmentCount): RestPlace
    {
        $this->apartmentCount = $apartmentCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getApartmentCount(): int
    {
        return $this->apartmentCount;
    }

    /**
     * @param string $contactPerson
     * @return RestPlace
     */
    public function setContactPerson(string $contactPerson): RestPlace
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson(): string
    {
        return $this->contactPerson;
    }

    /**
     * @param string $phone
     * @return RestPlace
     */
    public function setPhone(string $phone): RestPlace
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $address
     * @return RestPlace
     */
    public function setAddress(string $address): RestPlace
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param int $price
     * @return RestPlace
     */
    public function setPrice(int $price): RestPlace
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $landlord
     * @return RestPlace
     */
    public function setLandlord($landlord)
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return Landlord
     */
    public function getLandlord()
    {
        return $this->landlord;
    }
}