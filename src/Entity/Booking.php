<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 02.07.18
 * Time: 13:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var RestPlace
     * @ORM\ManyToOne(targetEntity="RestPlace", inversedBy="bookings")
     */
    private $restPlace;

    /**
     * @var int
     * @ORM\Column(type="integer", unique=false)
     */
    private $apartment;

    /**
     * @var Tenant
     * @ORM\ManyToOne(targetEntity="Tenant", inversedBy="bookings")
     */
    private $tenant;

    /**
     * @ORM\Column(type="string")
     */
    private $startDate;

    /**
     * @ORM\Column(type="string")
     */
    private $endDate;

    /**
     * @param RestPlace $restPlace
     * @return Booking
     */
    public function setRestPlace(RestPlace $restPlace): Booking
    {
        $this->restPlace = $restPlace;
        return $this;
    }

    /**
     * @return RestPlace
     */
    public function getRestPlace(): RestPlace
    {
        return $this->restPlace;
    }

    /**
     * @param Tenant $tenant
     * @return Booking
     */
    public function setTenant(Tenant $tenant): Booking
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Tenant
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @param mixed $startDate
     * @return Booking
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $endDate
     * @return Booking
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param int $apartment
     * @return Booking
     */
    public function setApartment(int $apartment): Booking
    {
        $this->apartment = $apartment;
        return $this;
    }

    /**
     * @return int
     */
    public function getApartment(): int
    {
        return $this->apartment;
    }
}