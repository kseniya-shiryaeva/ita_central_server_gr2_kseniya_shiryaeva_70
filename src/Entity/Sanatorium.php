<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 02.07.18
 * Time: 13:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SanatoriumRepository")
 */
class Sanatorium extends RestPlace
{
    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $treatment;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $excursion;

    /**
     * @param int $treatment
     * @return Sanatorium
     */
    public function setTreatment(int $treatment): Sanatorium
    {
        $this->treatment = $treatment;
        return $this;
    }

    /**
     * @return int
     */
    public function getTreatment(): int
    {
        return $this->treatment;
    }

    /**
     * @param int $excursion
     * @return Sanatorium
     */
    public function setExcursion(int $excursion): Sanatorium
    {
        $this->excursion = $excursion;
        return $this;
    }

    /**
     * @return int
     */
    public function getExcursion(): int
    {
        return $this->excursion;
    }


    public function __toArray() {
        return [
            'placeId' => $this->getId(),
            'placeName' => $this->getPlaceName(),
            'apartmentCount' => $this->getApartmentCount(),
            'contactPerson' => $this->getContactPerson(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'price' => $this->getPrice(),
            'type' => $this->getType(),
            'landlord' => $this->getLandlord()->getEmail(),
            'treatment' => $this->getTreatment(),
            'excursion' => $this->getExcursion()
        ];
    }
}