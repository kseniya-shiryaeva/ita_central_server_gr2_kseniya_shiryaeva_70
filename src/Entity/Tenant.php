<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends Client
{
    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $creditCard;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="tenant")
     */
    private $bookings;

    /**
     * @param mixed $creditCard
     * @return Tenant
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }
}
