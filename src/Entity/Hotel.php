<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 02.07.18
 * Time: 13:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel extends RestPlace
{
    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $kitchen;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $selfPlage;

    /**
     * @param int $kitchen
     * @return Hotel
     */
    public function setKitchen(int $kitchen): Hotel
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return int
     */
    public function getKitchen(): int
    {
        return $this->kitchen;
    }

    /**
     * @param int $selfPlage
     * @return Hotel
     */
    public function setSelfPlage(int $selfPlage): Hotel
    {
        $this->selfPlage = $selfPlage;
        return $this;
    }

    /**
     * @return int
     */
    public function getSelfPlage(): int
    {
        return $this->selfPlage;
    }


    public function __toArray() {
        return [
            'placeId' => $this->getId(),
            'placeName' => $this->getPlaceName(),
            'apartmentCount' => $this->getApartmentCount(),
            'contactPerson' => $this->getContactPerson(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'price' => $this->getPrice(),
            'type' => $this->getType(),
            'landlord' => $this->getLandlord()->getEmail(),
            'kitchen' => $this->getKitchen(),
            'selfPlage' => $this->getSelfPlage()
        ];
    }
}