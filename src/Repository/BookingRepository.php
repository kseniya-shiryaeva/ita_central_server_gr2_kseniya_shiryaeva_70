<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function findOneByTenant(string $tenant) : ?Booking
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.tenant = :tenant')
                ->setParameter('tenant', $tenant)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param int $restPlace
     * @return array
     */
    public function getDatesByRestPlace(int $restPlace)
    {
       return $this->createQueryBuilder('a')
                ->select('MAX(a.endDate) as date', 'a.apartment')
                ->where('a.restPlace = :restPlace')
                ->setParameter('restPlace', $restPlace)
                ->groupBy('a.apartment')
                ->getQuery()
                ->getArrayResult();
    }
}
