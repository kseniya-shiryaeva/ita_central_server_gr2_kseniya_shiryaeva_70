<?php
namespace App\DataFixtures;

use App\Model\RestPlace\RestPlaceHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RestPlaceFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var RestPlaceHandler
     */
    private $restPlaceHandler;

    public function __construct(RestPlaceHandler $restPlaceHandler)
    {
        $this->restPlaceHandler = $restPlaceHandler;
    }

    public function load(ObjectManager $manager)
    {
        $landlord1 = $this->getReference('client2');
        $landlord2 = $this->getReference('client3');

        $restPlace1 = $this->restPlaceHandler->createNewSanatorium([
            'landlord' => $landlord1,
            'placeName' => 'Голубой Иссык-Куль',
            'apartmentCount' => '7',
            'contactPerson' => 'Manager',
            'phone' => '222 555',
            'address' => 'Ленина, 12',
            'price' => '1200',
            'type' => 'sanatorium',
            'excursion' => 1,
            'treatment' => 1
        ]);

        $manager->persist($restPlace1);
        $this->addReference('restPlace1', $restPlace1);

        $restPlace2 = $this->restPlaceHandler->createNewHotel([
            'landlord' => $landlord2,
            'placeName' => 'Отель Радуга',
            'apartmentCount' => '5',
            'contactPerson' => 'Director',
            'phone' => '111 555',
            'address' => 'Мира, 12',
            'price' => '1000',
            'type' => 'hotel',
            'kitchen' => 1,
            'selfPlage' => 0
        ]);

        $manager->persist($restPlace2);
        $this->addReference('restPlace2', $restPlace2);

        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            ClientFixtures::class,

        );

    }
}