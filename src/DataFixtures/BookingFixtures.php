<?php
namespace App\DataFixtures;

use App\Entity\RestPlace;
use App\Model\Booking\BookingHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var BookingHandler
     */
    private $bookingHandler;

    public function __construct(BookingHandler $bookingHandler)
    {
        $this->bookingHandler = $bookingHandler;
    }

    public function load(ObjectManager $manager)
    {
        $tenant1 = $this->getReference('client1');
        $tenant2 = $this->getReference('client4');
        $tenant3 = $this->getReference('client5');
        $tenant4 = $this->getReference('client6');
        $tenant5 = $this->getReference('client7');
        $restPlace1 = $this->getReference('restPlace1');
        $restPlace2 = $this->getReference('restPlace2');

        $booking1 = $this->bookingHandler->createNewBooking([
            'restPlace' => $restPlace1,
            'apartment' => 1,
            'tenant' => $tenant1,
            'startDate' => '2018-06-01 00:00:00'
        ]);

        $manager->persist($booking1);

        $booking2 = $this->bookingHandler->createNewBooking([
            'restPlace' => $restPlace2,
            'apartment' => 1,
            'tenant' => $tenant2,
            'startDate' => '2018-06-01 00:00:00'
        ]);

        $manager->persist($booking2);

        $booking3 = $this->bookingHandler->createNewBooking([
            'restPlace' => $restPlace2,
            'apartment' => 1,
            'tenant' => $tenant3,
            'startDate' => '2018-06-08 00:00:00'
        ]);

        $manager->persist($booking3);

        $booking4 = $this->bookingHandler->createNewBooking([
            'restPlace' => $restPlace2,
            'apartment' => 1,
            'tenant' => $tenant4,
            'startDate' => '2018-06-15 00:00:00'
        ]);

        $manager->persist($booking4);

        $booking5 = $this->bookingHandler->createNewBooking([
            'restPlace' => $restPlace2,
            'apartment' => 2,
            'tenant' => $tenant5,
            'startDate' => '2018-06-01 00:00:00'
        ]);

        $manager->persist($booking5);

        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            ClientFixtures::class,
            RestPlaceFixtures::class,

        );

    }
}