<?php
namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client1 = $this->clientHandler->createNewTenant([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => 'pass_5678',
            'credit_card' => '555555',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client1);
        $this->addReference('client1', $client1);

        $client2 = $this->clientHandler->createNewLandlord([
            'email' => 'ewq@qwe.ru',
            'passport' => 'qwerty & some',
            'password' => 'qwerty',
            'full_name' => 'Vasya Dyadya',
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client2);
        $this->addReference('client2', $client2);

        $client3 = $this->clientHandler->createNewLandlord([
            'email' => 'lll@lll.ru',
            'passport' => 'passport 222',
            'password' => 'pass_5678',
            'full_name' => 'Tyetia Shura',
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ]);

        $manager->persist($client3);
        $this->addReference('client3', $client3);


        $client4 = $this->clientHandler->createNewTenant([
            'email' => 'ddd@ddd.ru',
            'passport' => 'passport 777',
            'password' => 'pass_87654',
            'credit_card' => '888888',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client4);
        $this->addReference('client4', $client4);


        $client5 = $this->clientHandler->createNewTenant([
            'email' => 'nnn@nnn.ru',
            'passport' => 'passport 999',
            'password' => 'pass_87654',
            'credit_card' => '888888',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client5);
        $this->addReference('client5', $client5);


        $client6 = $this->clientHandler->createNewTenant([
            'email' => 'zzz@zzz.ru',
            'passport' => 'passport 444',
            'password' => 'pass_87654',
            'credit_card' => '888888',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client6);
        $this->addReference('client6', $client6);


        $client7 = $this->clientHandler->createNewTenant([
            'email' => 'sss@sss.ru',
            'passport' => 'passport 000',
            'password' => 'pass_87654',
            'credit_card' => '888888',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $manager->persist($client7);
        $this->addReference('client7', $client7);

        $manager->flush();
    }
}