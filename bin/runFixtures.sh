#!/usr/bin/env bash

php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load -n
php bin/console api:organization:register -o"Рога и Копыта" -u"http://durakov.net"